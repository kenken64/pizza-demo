'use strict';

exports = module.exports = {
  // List of user roles
  userRoles: ['guest', 'user', 'admin'],

  apiEndPoint: 'http://168.63.139.127/eth/v1.2/',
  keyserver: 'http://localhost:8000/'
};
