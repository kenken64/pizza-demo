(function(angular, undefined) {
'use strict';

angular.module('pizzaSwapApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin'],apiEndPoint:'http://168.63.139.127/eth/v1.2/',keyserver:'http://localhost:8000/'})

;
})(angular);